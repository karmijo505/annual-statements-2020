<?php

use Illuminate\Database\Seeder;

class RisksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql_file = "risks.sql";

        // execute raw sql file (insert statements) placed in sql folder
        DB::unprepared(file_get_contents(database_path()."/seeds/sql/".$sql_file));
    }
}
