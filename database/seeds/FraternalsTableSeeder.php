<?php

use Illuminate\Database\Seeder;

class FraternalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql_file = "fraternals.sql";

        // execute raw sql file (insert statements) placed in sql folder
        DB::unprepared(file_get_contents(database_path()."/seeds/sql/".$sql_file));
    }
}
