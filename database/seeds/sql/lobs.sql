-- noinspection SqlNoDataSourceInspectionForFile

-- noinspection SqlDialectInspectionForFile

INSERT INTO lobs VALUES ('Accident and Health',1,49,200,100);
INSERT INTO lobs VALUES ('Casualty',2,49,200,100);
INSERT INTO lobs VALUES ('Property',3,73,200,100);
INSERT INTO lobs VALUES ('Life and Annuities',4,49,200,100);
INSERT INTO lobs VALUES ('Variable Life and Annuity',5,49,200,100);
INSERT INTO lobs VALUES ('Pre-Paid Dental',6,49,200,100);
INSERT INTO lobs VALUES ('Health Maintenance Organization',7,49,200,100);
INSERT INTO lobs VALUES ('Fraternal',8,86,100,100);
INSERT INTO lobs VALUES ('Non-Profit Health Care Plan',9,49,200,100);
