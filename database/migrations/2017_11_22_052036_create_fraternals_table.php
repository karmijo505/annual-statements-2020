<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFraternalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fraternals', function (Blueprint $table) {
            $table->string('company_name',255);
            $table->integer('naic_cocode');
            $table->string('mlg_address1',255);
            $table->string('mailing_city',255);
            $table->string('mailing_state',255);
            $table->string('mailing_zip',10);
            $table->integer('lob_id');

            $table->primary('naic_cocode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fraternal');
        Schema::dropIfExists('fraternals');
    }
}
