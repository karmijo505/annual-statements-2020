
var searchModule = (function() {

    var stateMap = {

            routePath : null,
            typeAhead : null,
            isCardShown : false
        },
        jqueryMap = {},
        initTypeAhead, setJqueryMap, initModule;

    initTypeAhead = function(input) {

        var companies = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('company_name', 'naic_cocode'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url : stateMap.routePath
            }
        });

        stateMap.typeAhead = input.typeahead({
            hint: false,
            highlight: true,
            minLength: 1
        },
        {
            name: 'companies',
            source: companies,
            displayKey: 'company_name',
            limit: 25,
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'Unable to match a company based on this search.',
                    '</div>'
                ].join('\n'),
                suggestion: function(data) {
                    return '<p>' + data.company_name + ' - ' + data.naic_cocode + '</p>';
                }
            }
        });
    };

    setJqueryMap = function(container) {

        $container = $(container);

        jqueryMap = {
            $container : $container,
            $searchSelect: $container.find("#filter-select"),
            $searchInput : $container.find("#search-input"),
            $resetButton : $container.find("#reset-button"),
            $detailsCard : $container.find("#details-card"),
            $pdfButton : $container.find("#company-pdf"),
            $companyDetails : $container.find("#js-company-details"),
            $mailingAddress : $container.find("#js-mailing-address"),
            $lineOfBusiness : $container.find("#js-lobs"),
            $tabs: $container.find(".tabs")
        }
    };

    initModule = function(container) {

        // INIT

        stateMap.routePath = 'companies';
        setJqueryMap(container);

        jqueryMap.$searchSelect.formSelect();
        jqueryMap.$tabs.tabs();

        initTypeAhead(jqueryMap.$searchInput);

        // EVENT BINDING
        stateMap.typeAhead.on("typeahead:selected", function(event, datum, dataset) {

            if (typeof datum == "undefined") { return;}
            var lobHtml = "";

            if (datum.lobs.length) {
                for (lob in datum.lobs) {
                    lobHtml += "<li>" + datum.lobs[lob].line_of_business + "</li>";
                }
            } else {
                lobHtml += "<li>" + datum.lobs.line_of_business +"</li>";
            }

            jqueryMap.$lineOfBusiness.html(lobHtml);
            jqueryMap.$companyDetails.html('<p>' + datum.company_name + '</p><p><span style="font-weight: bold">NAIC Code:</span> '
                + datum.naic_cocode + '</p>');
            jqueryMap.$mailingAddress.html('<p>' + datum.company_name + '</p><p>' + datum.mlg_address1 + '</p><p>'
                + datum.mailing_city+ ' ' + datum.mailing_state +' ' + datum.mailing_zip +'</p>');


            jqueryMap.$pdfButton.attr("href", "/pdf?entity=" + stateMap.routePath + "&naic_cocode=" + datum.naic_cocode);
            jqueryMap.$detailsCard.fadeIn(500);
            jqueryMap.$mailingAddress.click();
        });

        jqueryMap.$searchSelect.on("change", function() {

            stateMap.routePath = $(this).val();
            stateMap.typeAhead.typeahead('destroy');
            initTypeAhead(jqueryMap.$searchInput);
            stateMap.typeAhead.typeahead('val', '').focus();
            jqueryMap.$detailsCard.fadeOut(500);
        });

        jqueryMap.$searchInput.on('keydown', function(e) {
            if (e.which == 13 || jqueryMap.$detailsCard.not(":visible")) {
                return;
            }
            jqueryMap.$detailsCard.fadeOut(500);
        });

        jqueryMap.$resetButton.on("click", function() {
            stateMap.typeAhead.typeahead('val', '').focus();
            jqueryMap.$detailsCard.fadeOut(500);
        });
    };

    return {
        initModule : initModule
    }
}());

searchModule.initModule("#clrewnew-search");