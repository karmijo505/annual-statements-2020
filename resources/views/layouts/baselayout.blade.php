<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {!! Html::style('css/app.css') !!}

</head>

<body>

<nav class="grey darken-4">
    <div class="nav-wrapper">
        <a href="/" class="brand-logo center">{{ config('app.name') }}</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
        </ul>
    </div>
</nav>

<div style="text-align:center; margin-top:20px; font-size: 22px"> NM Office of Superintendent of Insurance</div>
<div style="text-align:center">
    <img src="/images/osi-transparent.png" width="125px" height="80px" style="margin-bottom:15px">
</div>

@yield('content')


<!-- Scripts -->
{!! Html::script('js/app.js') !!}

</body>
</html>