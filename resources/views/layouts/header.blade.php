<style>
    .headcenter {
        text-align: center;
        color: red;
    }
    .headcenterBold {
        text-align: center;
        color: red;
    }
    .fullborder {
        background-color: #FFFFFF;
        border: 4px double red;
        padding: 30px;
    }
    .t-center {text-align: center }
    @page {
        margin: 20px;
    }
    #column-left {
        width: 300px;
        float: left;
    }
    #column-center {
        width: 300px;
        float: left;
    }
    #column-right {
        width: 300px;
        float: left;
    }
    .w-100 { width: 100% }
    .w-60 { width: 60%; }
    .w-55 { width: 55%; }
    .w-45 { width: 45%; }
    .w-40 { width: 40%; }
    .w-35 { width: 35%; }
    .w-30 { width: 30%; }
    .w-25 { width: 25%; }
    .w-20 { width: 20%; }
    .f-left { float: left; }
    .f-right { float: right; }
</style>
<html>
<body>
<div class="fullborder">

<h2 class="headcenter">STATE OF NEW MEXICO<br>
OFFICE OF SUPERINTENDENT OF INSURANCE</h2>
<div style="margin-bottom:15px">
    <div class="f-left w-25">
        <div class="headcenter">SUPERINTENDENT</div>
        <div class="headcenter">OF INSURANCE</div>
        <div class="headcenter">Russell Toal</div>
    </div>
    <div class="f-left w-45 headcenter"><img src="/images/osi-transparent.png" width="85px" height="60px" style="margin-bottom:15px">
{{--        <div class="headcenterBold">SUPERINTENDENT OF INSURANCE</div>
        <div class="headcenter">Russell Toal</div>--}}
    </div>
    <div class="f-right w-25 headcenter">
        <div class="headcenter">DEPUTY SUPERINTENDENT</div>
        <div class="headcenter">Robert E. Doucette, Jr.</div>
    </div>
</div>



    @yield ('content')
</div>
</body>
</html>