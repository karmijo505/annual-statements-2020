<html>
<head>
    <style>
        .headcenter {
            text-align: center;
            color: red;
        }

        .w-100 {
            width: 100%;
        }

        .w-75 {
            width: 75%;
        }

        .w-65 {
            width: 65%;
        }

        .w-60 {
            width: 60%;
        }

        .w-55 {
            width: 55%;
        }

        .w-50 {
            width: 50%;
        }

        .w-45 {
            width: 45%;
        }

        .w-40 {
            width: 40%;
        }

        .w-35 {
            width: 35%;
        }

        .w-30 {
            width: 30%;
        }

        .w-20 {
            width: 20%;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

        .t-left {
            text-align: left;
        }

        .t-right {
            text-align: right;
        }

        .t-center {
            text-align: center
        }

        .t-size10 {
            font-size: 1.10em;
        }

        .t-size12 {
            font-size: 12px;
        }

        .center {
            margin: 0 auto;
        }

        .bold {
            font-weight: bold;
        }

        .company-info {
            padding: 10px;
        }

        .notification-info {
            padding: 35px 0;
        }

        .office-use-info {
            padding: 25px;
        }

        .foot {
            position: fixed;
            text-align: center;
            width: 100%;
            padding: 10px;
            bottom: 0;
        }

        table th {
            border: 1px solid #444;
        }

        fieldset {
            border: 1px solid #444;
            padding: 15px;
            width: 50%;
        }

        @page {
            margin-top: 25px;
            margin-bottom: 25px;
            margin-left: 25px;
            margin-right: 25px;
        }
    </style>
</head>


@extends('layouts.header')
@section('content')

    <div>
        <div class="company-info f-left w-45 t-size12">
            <div>NAIC #: {{ $entityData['naic_cocode'] }}</div>
            <div>{{ $entityData['company_name'] }}</div>
            <div>{{ $entityData['mlg_address1'] }}</div>
            <span>{{ $entityData['mailing_city'] }}</span>
            <span>{{ $entityData['mailing_state'] }}, </span>
            <span>{{ $entityData['mailing_zip'] }}</span>
        </div>

        <div class="f-right w-45 t-left">
            <div class="company-info f-left w-45 t-left t-size12">
                <div>Date of this Notice</div>
                <div>Taxable Year Affected</div>
                <div>Total Amount Due By</div>
            </div>
            <div class="company-info f-right w-30 t-left t-size12">
                <div>{{ $noticeDate }}</div>
                <div>{{ $taxableYear }}</div>
                <div>{{ $dueDate }}</div>
            </div>
        </div>
    </div>

<h3 class="t-center">NOTICE OF ANNUAL CONTINUATION OF CERTIFICATE OF AUTHORITY</h3>

<table width="100%">
    <tr>
        <th>Description</th>
        <th>Amount</th>
    </tr>
    @foreach ($entityData['lobs'] as $lob)
    <tr>
        <td class="t-left">{{  $lob['line_of_business'] }}</td>
        <td class="t-center">{{ money_format('%.2n', $lob['cost']) }}</td>
    </tr>
    @endforeach
    <tr>
        <td class="t-right">Total amount to be Remitted:</td>
        <td class="t-center bold">{{ money_format('%.2n', $totalAmt) }}</td>
    </tr>
</table>

<div class="notification-info w-100 t-left t-size10">
    In accordance with NMSA 1978 Section 59A-44-36 (A-1) or 59A-44-36 [B-4(A)] as amended, you are hereby notified the Fraternal Benefit Society's annual continuation of certificate of authority <span class="bold">fee of $50.00 or $100.00</span> is due on <span class="bold">{{ $dueDate }}</span>.
</div>

<div class="payment-info">
    <h3 class="t-center">Please Return Copy of This Notice With Your Payment</h3>
    <h3 class="t-center">Make Check Payable to:</h3>
</div>

    <div>
        <div class="f-left w-35">
            <div class="t-left t-size10 bold">NM OSI</div>
            <div class="f-left w-100 t-left t-size12">
                <div>Company Licensing Bureau</div>
                <div>P.O. Box 1689</div>
                <div>Santa Fe, New Mexico 87504-1689</div>
                <div>(505)827-4362</div>
            </div>
        </div>

        <div class="f-left w-65">
            <div class="w-100 f-left t-left t-size10">
                <fieldset class="t-left w-100">
                    <legend>For OSI Use Only</legend>
                    Check No.
                    <hr/>
                    Amount Remitted $
                    <hr/>
                </fieldset>
            </div>
        </div>
    </div>
    <div class="foot" style="margin-top:120px">
        <img src="/images/osi-transparent.png" width="125px" height="80px">
    </div>
    <div class="headcenter t-size12" style="margin-bottom: -30px">
        <div>Main Office: 1120 Paseo de Peralta, Room 428, Santa Fe, NM 87501</div>
        <div>Satellite Office: 6200 Uptown Blvd NE, Suite 100, Albuquerque, NM 87110</div>
        <div>Main Phone: (505) 827-4601 | Satellite Phone: (505) 322-2186 | Toll Free: (855) 4-ASK-OSI</div>
        <div>www.osi.state.nm.us</div>
    </div>
    @endsection
    </body>
</html>
