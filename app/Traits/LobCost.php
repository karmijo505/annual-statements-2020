<?php

namespace App\Traits;

trait LobCost
{

    // business rule: set single (lob) cost or multi (lob) cost for each lob
    public function setLobCost($lobs)
    {
        $first49Key = null;
        $hasNon49Code = $this->hasNon49Code($lobs); // todo: ugly - need to refactor

        foreach ($lobs as $key => $value) {

            if (count($lobs) == 1 || $lobs[$key]['account_code'] == 86) {

                $lobs[$key]['cost']  = $lobs[$key]["single_line_cost"];
            }
            else if ($lobs[$key]['account_code'] == 49) {

                $first49Key = is_null($first49Key) ?
                    $key :
                    $first49Key;

                $lobs[$first49Key]['cost'] = $hasNon49Code ?
                    $lobs[$first49Key]['multi_line_cost'] :
                    $lobs[$first49Key]['single_line_cost'];

                if ($key != $first49Key)
                    $lobs[$key]['cost'] = 0;

                continue;
            }
            else {
                $lobs[$key]['cost']  = $lobs[$key]["multi_line_cost"];
            }
        }
        return $lobs;
    }

    // helper to check for first non 49 code in lobs
    public function hasNon49Code($lobs) {

        foreach ($lobs as $key => $value) {
            if ($lobs[$key]['account_code'] != 49) return true;
        }
        return false;
    }
}