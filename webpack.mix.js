let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/typeahead.js/dist/typeahead.bundle.js',
        './node_modules/jquery.growl/javascripts/jquery.growl.js',
        './node_modules/materialize-css/dist/js/materialize.js',
        'resources/assets/js/app.js'
    ], 'public/js/app.js');

mix.sass('resources/assets/sass/app.scss', 'public/css');
